package io.github.arraylists.cosmetics.commands;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.menus.CosmeticMenu;
import io.github.arraylists.cosmetics.menus.GadgetsMenu;
import io.github.arraylists.cosmetics.menus.SuitsMenu;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FunCosmeticsCommand implements CommandExecutor {

    private Main plugin;

    public FunCosmeticsCommand(Main Main2) {
        this.plugin = Main2;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("not-a-player")));
            return true;
        }

        Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("funcosmetics")) {
            if (!sender.hasPermission("funcosmetics.use")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("no-perms")));
                return true;
            }

            if (args.length == 0) {
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-1")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-2")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-3")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-4")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-5")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-6")));
                return true;
            }

            if (args[0].equalsIgnoreCase("open")) {
                if (args.length < 2) {
                    CosmeticMenu.getInstance().openCosmetics(player);
                    return true;
                }
                if (args[1].equalsIgnoreCase("gadgets") || args[1].equalsIgnoreCase("gadget")) {
                    GadgetsMenu.getInstance().openGadgets(player);
                    return true;
                } else if (args[1].equalsIgnoreCase("suits") || args[1].equalsIgnoreCase("suit")) {
                    SuitsMenu.getInstance().openSuits(player);
                    return true;
                } else if (!args[1].equalsIgnoreCase("suits") || !args[1].equalsIgnoreCase("suit") || !args[1].equalsIgnoreCase("gadgets") || !args[1].equalsIgnoreCase("gadget")) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-1")));
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-2")));
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-3")));
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-4")));
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-5")));
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-6")));
                    return true;
                }
                return true;
            } else if (args[0].equalsIgnoreCase("list")) {
                return true;
            } else if (args[0].equalsIgnoreCase("reload")) {
                if (player.hasPermission("funcosmetics.reload")) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', "&b&lFunCosmetics &8» &bReloading config..."));
                    this.plugin.saveConfig();
                    this.plugin.reloadConfig();
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', "&b&lFunCosmetics &8» &bSuccess!"));
                    return true;
                } else {
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("no-perms")));
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("admin")) {
                if (player.hasPermission("funcosmetics.admin")) {
                    // TODO: Print admin usage.
                } else {
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("no-perms")));
                    return true;
                }
            } else if (!args[0].equalsIgnoreCase("open") || !args[0].equalsIgnoreCase("list") || !args[0].equalsIgnoreCase("reload") || !args[0].equalsIgnoreCase("admin") || !args[0].equalsIgnoreCase("toggle")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-1")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-2")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-3")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-4")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-5")));
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("usage-info-line-6")));
                return true;
            } else if (args[0].equalsIgnoreCase("toggle")) {
                if (this.plugin.gadgetsToggled.contains(player.getName())) {
                    this.plugin.gadgetsToggled.remove(player.getName());
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("gadgets-toggled-off")));
                    return true;
                } else {
                    this.plugin.gadgetsToggled.add(player.getName());
                    player.sendMessage(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("gadgets-toggled-on")));
                    return true;
                }
            }
        }
        return true;
    }
}
