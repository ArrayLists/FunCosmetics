package io.github.arraylists.cosmetics.listeners;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.menus.CosmeticMenu;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class onInteract implements Listener {

    private Main plugin;

    public onInteract(Main Main2) {
        this.plugin = Main2;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        if (event.getItem().getType() != Material.NETHER_STAR) {
            return;
        }
        if (
                !event.getItem().hasItemMeta() ||
                        !event.getItem().getItemMeta().hasDisplayName() ||
                        !event.getItem().getItemMeta().hasLore()
                ) {
            return;
        }

        CosmeticMenu.getInstance().openCosmetics(event.getPlayer());
    }
}
