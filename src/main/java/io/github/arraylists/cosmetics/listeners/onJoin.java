package io.github.arraylists.cosmetics.listeners;

import io.github.arraylists.cosmetics.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class onJoin implements Listener {

    private Main plugin;

    public onJoin(Main Main2) {
        this.plugin = Main2;
    }

    public void giveCosmeticSelector(Player player) {
        ItemStack cosmeticSelector = new ItemStack(Material.NETHER_STAR);
        ItemMeta cosmeticSelectorMeta = cosmeticSelector.getItemMeta();

        ArrayList<String> cosmeticSelectorLore = new ArrayList<String>();
        cosmeticSelectorMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("cosmetic-selector-name")));
        cosmeticSelectorLore.add(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("cosmetic-selector-lore")));
        cosmeticSelectorMeta.setLore(cosmeticSelectorLore);
        cosmeticSelector.setItemMeta(cosmeticSelectorMeta);

        player.getInventory().setItem(this.plugin.getConfig().getInt("cosmetic-selector-slot"), cosmeticSelector);
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        if (this.plugin.getConfig().getBoolean("give-cosmetic-selector-on-join") == true) {
            giveCosmeticSelector(event.getPlayer());
            return;
        } else {
            plugin.getLogger().info("Due to the fact that the cosmetic selector on join is disabled, the player was not given the cosmetic selector.");
            return;
        }
    }
}
