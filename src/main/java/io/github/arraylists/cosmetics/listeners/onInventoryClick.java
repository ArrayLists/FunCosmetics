package io.github.arraylists.cosmetics.listeners;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.menus.*;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class onInventoryClick implements Listener {

    private Main plugin;

    public onInventoryClick(Main Main2) {
        this.plugin = Main2;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (event.getInventory().getTitle().equals("Cosmetics")) {
            event.setCancelled(true);
        }
        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.BLAZE_ROD && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("cosmetics-gadgets-inv-name")))) {
            player.closeInventory();
            GadgetsMenu.getInstance().openGadgets(player);
            return;
        } else if (event.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("cosmetics-suits-inv-name")))) {
            player.closeInventory();
            SuitsMenu.getInstance().openSuits(player);
            return;
        } else if (event.getCurrentItem().getType() == Material.SKULL_ITEM && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("cosmetics-hats-inv-name")))) {
            player.closeInventory();
            HatsMenu.getInstance().openHats(player);
        } else if (event.getCurrentItem().getType() == Material.BANNER && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("cosmetics-banners-inv-name")))) {
            player.closeInventory();
            BannersMenu.getInstance().openBanners(player);
        }
    }

    @EventHandler
    public void onInventoryClickGadgets(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (event.getInventory().getTitle().contains("Gadgets")) {
            event.setCancelled(true);
        }
        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.ARROW && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("go-back-name-in-inv")))) {
            player.closeInventory();
            CosmeticMenu.getInstance().openCosmetics(player);
            return;
        } else if (event.getCurrentItem().getType() == Material.REDSTONE && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', this.plugin.getConfig().getString("reset-gadgets-name-in-inv")))) {
            player.closeInventory();
            player.getInventory().setItem(this.plugin.getConfig().getInt("gadget-slot"), null);
            return;
        }
    }
}