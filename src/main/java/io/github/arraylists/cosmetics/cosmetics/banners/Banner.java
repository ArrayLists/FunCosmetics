package io.github.arraylists.cosmetics.cosmetics.banners;


import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.entity.Player;

public abstract class Banner {

    public abstract CosmeticType getCosmeticType();
    public abstract String getName();
    public abstract void giveBanner(Player player);
}
