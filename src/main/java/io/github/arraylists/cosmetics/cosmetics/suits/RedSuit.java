package io.github.arraylists.cosmetics.cosmetics.suits;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class RedSuit extends Suit implements Listener {

    public CosmeticType getCosmeticType() {
        return CosmeticType.SUIT;
    }

    public String getHelmetName() {
        return "red helmet";
    }

    public String getChestplateName() {
        return "red chestplate";
    }

    public String getLeggingsName() {
        return "red leggings";
    }

    public String getBootsName() {
        return "red boots";
    }

    public void giveHelmet(Player player) {
        ItemStack redHelmet = new ItemStack(Material.LEATHER_HELMET);
        LeatherArmorMeta redHelmetMeta = (LeatherArmorMeta) redHelmet.getItemMeta();

        redHelmetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-helmet-name")));
        redHelmetMeta.setColor(Color.RED);
        redHelmet.setItemMeta(redHelmetMeta);

        player.getInventory().setHelmet(redHelmet);
    }

    public void giveChestplate(Player player) {
        ItemStack redChestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta redChestplateMeta = (LeatherArmorMeta) redChestplate.getItemMeta();

        redChestplateMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-chestplate-name")));
        redChestplateMeta.setColor(Color.RED);
        redChestplate.setItemMeta(redChestplateMeta);

        player.getInventory().setChestplate(redChestplate);
    }

    public void giveLeggings(Player player) {
        ItemStack redLeggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta redLeggingsMeta = (LeatherArmorMeta) redLeggings.getItemMeta();

        redLeggingsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-leggings-name")));
        redLeggingsMeta.setColor(Color.RED);
        redLeggings.setItemMeta(redLeggingsMeta);

        player.getInventory().setLeggings(redLeggings);
    }

    public void giveBoot(Player player) {
        ItemStack redBoots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta redBootsMeta = (LeatherArmorMeta) redBoots.getItemMeta();

        redBootsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-boots-name")));
        redBootsMeta.setColor(Color.RED);
        redBoots.setItemMeta(redBootsMeta);

        player.getInventory().setBoots(redBoots);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals("Suits")) {
            event.setCancelled(true);
        }

        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.LEATHER_HELMET && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-helmet-name-in-inv")))) {
            if (player.hasPermission("funcosmetics.suits.redhelmet")) {
                player.closeInventory();
                giveHelmet(player);
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("on-helmet-select").replaceAll("%helmet%", getHelmetName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("helmet-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        } else if (event.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-chestplate-name-in-inv")))) {
            if (player.hasPermission("funcosmetics.suits.redchestplate")) {
                player.closeInventory();
                giveChestplate(player);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("on-chestplate-select").replaceAll("%chestplate%", getChestplateName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("chestplate-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        } else if (event.getCurrentItem().getType() == Material.LEATHER_LEGGINGS && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-leggings-name-in-inv")))) {
            if (player.hasPermission("funcosmetics.suits.redleggings")) {
                player.closeInventory();
                giveLeggings(player);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("on-leggings-select").replaceAll("%leggings%", getLeggingsName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("leggings-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        } else if (event.getCurrentItem().getType() == Material.LEATHER_BOOTS && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-boots-name-in-inv")))) {
            if (player.hasPermission("funcosmetics.suits.redboots")) {
                player.closeInventory();
                giveBoot(player);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("on-boots-select").replaceAll("%boots%", getBootsName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("boots-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        }
    }
}
