package io.github.arraylists.cosmetics.cosmetics;

public enum CosmeticType {

    GADGET, SUIT, BANNER, MORPH, PET, HAT

    // TODO: Finish off mounts, morphs and pets.
}
