package io.github.arraylists.cosmetics.cosmetics.suits;

import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class Suit {

    public abstract CosmeticType getCosmeticType();
    public abstract String getHelmetName();
    public abstract String getChestplateName();
    public abstract String getLeggingsName();
    public abstract String getBootsName();
    public abstract void giveHelmet(Player player);
    public abstract void giveChestplate(Player player);
    public abstract void giveLeggings(Player player);
    public abstract void giveBoot(Player player);
}
