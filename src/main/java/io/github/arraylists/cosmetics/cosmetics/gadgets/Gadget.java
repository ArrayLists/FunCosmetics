package io.github.arraylists.cosmetics.cosmetics.gadgets;

import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.entity.Player;

public abstract class Gadget {

    public abstract CosmeticType getCosmeticType();
    public abstract String getName();
    public abstract void runCooldown(Player player);
    public abstract void giveGadget(Player player);
}
