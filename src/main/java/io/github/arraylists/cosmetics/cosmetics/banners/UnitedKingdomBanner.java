package io.github.arraylists.cosmetics.cosmetics.banners;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import java.util.ArrayList;
import java.util.List;

public class UnitedKingdomBanner extends Banner implements Listener {

    public CosmeticType getCosmeticType() {
        return CosmeticType.BANNER;
    }

    public String getName() {
        return "United Kingdom";
    }

    public void giveBanner(Player player) {
        ItemStack ukBanner = new ItemStack(Material.BANNER);
        BannerMeta ukBannerMeta = (BannerMeta) ukBanner.getItemMeta();

        List<Pattern> patterns = new ArrayList<Pattern>();

        ukBannerMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("uk-banner-name")));
        ukBannerMeta.setBaseColor(DyeColor.BLUE);
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_DOWNRIGHT));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_DOWNLEFT));
        patterns.add(new Pattern(DyeColor.RED, PatternType.CROSS));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_CENTER));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_MIDDLE));
        patterns.add(new Pattern(DyeColor.RED, PatternType.STRAIGHT_CROSS));
        ukBannerMeta.setPatterns(patterns);
        ukBanner.setItemMeta(ukBannerMeta);

        player.getInventory().setHelmet(ukBanner);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals("Banners")) {
            event.setCancelled(true);
        }
        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.BANNER && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("uk-banner-name-in-inv")))) {
            player.closeInventory();
            giveBanner(player);
            player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("on-banner-select").replaceAll("%banner%", getName())));
            player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("banner-select-sound")), 1, 1);
            return;
        }
    }
}
