package io.github.arraylists.cosmetics.cosmetics.morphs;

import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.entity.Player;

public abstract class Morph {

    public abstract CosmeticType getCosmeticType();
    public abstract String getName();
    public abstract void applyMorph(Player player);
}
