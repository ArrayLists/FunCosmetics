package io.github.arraylists.cosmetics.cosmetics.hats;

import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Hat {

    public abstract CosmeticType getCosmeticType();
    public abstract String getName();
    public abstract void giveHat(Player player);
}
