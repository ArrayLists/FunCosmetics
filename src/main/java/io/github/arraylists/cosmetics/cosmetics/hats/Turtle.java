package io.github.arraylists.cosmetics.cosmetics.hats;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import io.github.arraylists.cosmetics.utils.HatGenerator;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.UUID;

public class Turtle extends Hat implements Listener {

    public CosmeticType getCosmeticType() {
        return CosmeticType.HAT;
    }

    public String getName() {
        return "turtle";
    }

    public void giveHat(Player player) {
        ItemStack turtleHat = HatGenerator.getInstance().createHat("http://textures.minecraft.net/texture/12e548408ab75d7df8e6d5d2446d90b6ec62aa4f7feb7930d1ee71eefddf6189");
        SkullMeta turtleHatMeta = (SkullMeta) turtleHat.getItemMeta();

        turtleHatMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("turtlehat-name")));
        turtleHat.setItemMeta(turtleHatMeta);

        player.getInventory().setHelmet(turtleHat);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals("Hats")) {
            event.setCancelled(true);
        }
        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.SKULL_ITEM && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("turtlehat-name-in-inv")))) {
            if (player.hasPermission("funcosmetics.hats.turtle")) {
                giveHat(player);
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("on-hat-select").replaceAll("%hat%", getName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("hat-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        }
    }
}
