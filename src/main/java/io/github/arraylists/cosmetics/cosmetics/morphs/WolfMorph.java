package io.github.arraylists.cosmetics.cosmetics.morphs;

import de.robingrether.idisguise.disguise.DisguiseType;
import de.robingrether.idisguise.disguise.WolfDisguise;
import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class WolfMorph extends Morph implements Listener {

    public CosmeticType getCosmeticType() {
        return CosmeticType.MORPH;
    }

    public String getName() {
        return "wolf morph";
    }

    public void applyMorph(Player player) {
        WolfDisguise disguise = (WolfDisguise) DisguiseType.WOLF.newInstance();
        disguise.setCollarColor(DyeColor.BLUE);
        disguise.setAdult(true);
        Main.getInstance().api.disguise(player, disguise);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (event.getInventory().getTitle().equals("Morphs")) {
            event.setCancelled(true);
        }
        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.BONE && event.getCurrentItem().getItemMeta().getDisplayName().equals("wolfmorph-name-in-inv")) {
            if (player.hasPermission("funcosmetics.morphs.wolf")) {
                applyMorph(player);
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("on-morph-select").replaceAll("%morph%", getName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("morph-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        }
    }
}
