package io.github.arraylists.cosmetics.cosmetics.gadgets;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class Launcher extends Gadget implements Listener {

    public CosmeticType getCosmeticType() {
        return CosmeticType.GADGET;
    }

    public String getName() {
        return "launcher";
    }

    public void runCooldown(Player player) {

    }

    public void giveGadget(Player player) {
        ItemStack launcherGadget = new ItemStack(Material.FIREWORK_CHARGE);
        ItemMeta launcherGadgetMeta = launcherGadget.getItemMeta();

        ArrayList<String> launcherGadgetLore = new ArrayList<String>();
        launcherGadgetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("launcher-gadget-name")));
        launcherGadgetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("launcher-gadget-lore")));
        launcherGadgetMeta.setLore(launcherGadgetLore);
        launcherGadget.setItemMeta(launcherGadgetMeta);

        player.getInventory().setItem(Main.getInstance().getConfig().getInt("gadget-slot"), launcherGadget);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals("Gadgets")) {
            event.setCancelled(true);
        }

        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.FIREWORK_CHARGE && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("launcher-gadget-name-in-inv")))) {
            if (player.hasPermission("funcosmetics.gadgets.launcher")) {
                player.closeInventory();
                giveGadget(player);
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("on-gadget-select").replaceAll("%gadget%", getName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("gadget-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        Entity target = event.getRightClicked();


        if (player.getItemInHand().getType() != Material.FIREWORK_CHARGE) {
            return;
        }
        if (
                !player.getItemInHand().hasItemMeta() ||
                        !player.getItemInHand().getItemMeta().hasDisplayName() ||
                        !player.getItemInHand().getItemMeta().hasLore()
                ) {
            return;
        }
        if (Main.getInstance().gadgetsToggled.contains(target.getName())) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("target-gadgets-toggled").replaceAll("%target%", target.getName())));
            return;
        }

        if (target instanceof Player) {
            ((Player) target).playSound(target.getLocation(), Sound.FIREWORK_LAUNCH, 1, 1);
            target.setVelocity(target.getLocation().getDirection().multiply(5));
            target.setVelocity(new Vector(target.getVelocity().getX(), 1.0D, target.getVelocity().getZ()));
        }
        target.setVelocity(target.getLocation().getDirection().multiply(5));
        target.setVelocity(new Vector(target.getVelocity().getX(), 1.0D, target.getVelocity().getZ()));
        return;
    }
}
