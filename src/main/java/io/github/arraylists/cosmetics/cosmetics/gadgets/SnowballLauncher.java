package io.github.arraylists.cosmetics.cosmetics.gadgets;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.cosmetics.CosmeticType;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class SnowballLauncher extends Gadget implements Listener {
    public CosmeticType getCosmeticType() {
        return CosmeticType.GADGET;
    }

    public String getName() {
        return "snowball launcher";
    }

    public void runCooldown(Player player) {

    }

    public void giveGadget(Player player) {
        ItemStack snowballLauncherGadget = new ItemStack(Material.GOLD_HOE);
        ItemMeta snowballLauncherGadgetMeta = snowballLauncherGadget.getItemMeta();

        ArrayList<String> snowballLauncherGadgetLore = new ArrayList<String>();
        snowballLauncherGadgetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("snowballlauncher-gadget-name")));
        snowballLauncherGadgetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("snowballlauncher-gadget-lore")));
        snowballLauncherGadgetMeta.setLore(snowballLauncherGadgetLore);
        snowballLauncherGadget.setItemMeta(snowballLauncherGadgetMeta);
        player.getInventory().setItem(Main.getInstance().getConfig().getInt("gadget-slot"), snowballLauncherGadget);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals("Gadgets")) {
            event.setCancelled(true);
        }

        if (event.getCurrentItem() == null) {
            return;
        } else if (event.getCurrentItem().getType() == Material.GOLD_HOE && event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("snowballlauncher-gadget-name-in-inv")))) {
            if (player.hasPermission("funcosmetics.gadgets.snowballlauncher")) {
                player.closeInventory();
                giveGadget(player);
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("on-gadget-select").replaceAll("%gadget%", getName())));
                player.playSound(player.getLocation(), Sound.valueOf(Main.getInstance().getConfig().getString("gadget-select-sound")), 1, 1);
                return;
            } else {
                player.closeInventory();
                player.sendMessage(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("no-perms")));
                return;
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        World world = player.getWorld();

        if (player.getItemInHand().getType() != Material.GOLD_HOE) {
            return;
        }
        if (
                !player.getItemInHand().hasItemMeta() ||
                        !player.getItemInHand().getItemMeta().hasDisplayName() ||
                        !player.getItemInHand().getItemMeta().hasLore()
                ) {
            return;
        }
        player.launchProjectile(Snowball.class);
        world.playSound(player.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
    }
}
