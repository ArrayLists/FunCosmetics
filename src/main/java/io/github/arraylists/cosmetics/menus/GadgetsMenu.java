package io.github.arraylists.cosmetics.menus;

import io.github.arraylists.cosmetics.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class GadgetsMenu {

    private GadgetsMenu() { }

    private static GadgetsMenu instance = new GadgetsMenu();

    public static GadgetsMenu getInstance() {
        return instance;
    }

    public Inventory gadgetsInv;

    public void openGadgets(Player player) {
        gadgetsInv = Bukkit.getServer().createInventory(null, Main.getInstance().getConfig().getInt("gadgets-inv-number-of-inventory-slots"), "Gadgets");

        ItemStack launcherGadget = new ItemStack(Material.FIREWORK_CHARGE);
        ItemMeta launcherGadgetMeta = launcherGadget.getItemMeta();

        ArrayList<String> launcherGadgetLore = new ArrayList<String>();
        launcherGadgetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("launcher-gadget-name-in-inv")));
        launcherGadgetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("launcher-gadget-lore-in-inv")));
        launcherGadgetMeta.setLore(launcherGadgetLore);
        launcherGadget.setItemMeta(launcherGadgetMeta);

        ItemStack snowballLauncherGadget = new ItemStack(Material.GOLD_HOE);
        ItemMeta snowballLauncherGadgetMeta = snowballLauncherGadget.getItemMeta();

        ArrayList<String> snowballLauncherGadgetLore = new ArrayList<String>();
        snowballLauncherGadgetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("snowballlauncher-gadget-name-in-inv")));
        snowballLauncherGadgetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("snowballlauncher-gadget-lore-in-inv")));
        snowballLauncherGadgetMeta.setLore(snowballLauncherGadgetLore);
        snowballLauncherGadget.setItemMeta(snowballLauncherGadgetMeta);

        ItemStack reset = new ItemStack(Material.REDSTONE);
        ItemMeta resetMeta = reset.getItemMeta();

        ArrayList<String> resetLore = new ArrayList<String>();
        resetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("reset-gadgets-name-in-inv")));
        resetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("reset-gadgets-lore-in-inv")));
        resetMeta.setLore(resetLore);
        reset.setItemMeta(resetMeta);

        ItemStack goBack = new ItemStack(Material.ARROW);
        ItemMeta goBackMeta = goBack.getItemMeta();

        ArrayList<String> goBackLore = new ArrayList<String>();
        goBackMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("go-back-name-in-inv")));
        goBackLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("go-back-lore-in-inv")));
        goBackMeta.setLore(goBackLore);
        goBack.setItemMeta(goBackMeta);

        gadgetsInv.setItem(0, launcherGadget);
        gadgetsInv.setItem(1, snowballLauncherGadget);
        gadgetsInv.setItem(Main.getInstance().getConfig().getInt("reset-gadgets-slot-in-inv"), reset);
        gadgetsInv.setItem(Main.getInstance().getConfig().getInt("gadgets-go-back-slot-in-inv"), goBack);
        player.openInventory(gadgetsInv);
    }
}
