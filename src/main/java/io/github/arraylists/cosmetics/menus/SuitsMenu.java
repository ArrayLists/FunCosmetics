package io.github.arraylists.cosmetics.menus;

import io.github.arraylists.cosmetics.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;

public class SuitsMenu {

    private SuitsMenu() {  }

    private static SuitsMenu instance = new SuitsMenu();

    public static SuitsMenu getInstance() {
        return instance;
    }

    public Inventory suitsInv;

    public void openSuits(Player player) {
        suitsInv = Bukkit.getServer().createInventory(null, Main.getInstance().getConfig().getInt("suits-inv-number-of-inventory-slots"), "Suits");

        ItemStack redHelmet = new ItemStack(Material.LEATHER_HELMET);
        LeatherArmorMeta redHelmetMeta = (LeatherArmorMeta) redHelmet.getItemMeta();

        ArrayList<String> redHelmetLore = new ArrayList<String>();
        redHelmetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-helmet-lore-in-inv")));
        redHelmetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-helmet-name-in-inv")));
        redHelmetMeta.setColor(Color.RED);
        redHelmetMeta.setLore(redHelmetLore);
        redHelmet.setItemMeta(redHelmetMeta);

        ItemStack redChestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta redChestplateMeta = (LeatherArmorMeta) redChestplate.getItemMeta();

        ArrayList<String> redChestplateLore = new ArrayList<String>();
        redChestplateLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-chestplate-lore-in-inv")));
        redChestplateMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-chestplate-name-in-inv")));
        redChestplateMeta.setColor(Color.RED);
        redChestplateMeta.setLore(redChestplateLore);
        redChestplate.setItemMeta(redChestplateMeta);

        ItemStack redLeggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta redLeggingsMeta = (LeatherArmorMeta) redLeggings.getItemMeta();

        ArrayList<String> redLeggingsLore = new ArrayList<String>();
        redLeggingsLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-leggings-lore-in-inv")));
        redLeggingsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-leggings-name-in-inv")));
        redLeggingsMeta.setColor(Color.RED);
        redLeggingsMeta.setLore(redLeggingsLore);
        redLeggings.setItemMeta(redLeggingsMeta);

        ItemStack redBoots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta redBootsMeta = (LeatherArmorMeta) redBoots.getItemMeta();

        ArrayList<String> redBootsLore = new ArrayList<String>();
        redBootsLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-boots-lore-in-inv")));
        redBootsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("redsuit-boots-name-in-inv")));
        redBootsMeta.setColor(Color.RED);
        redBootsMeta.setLore(redBootsLore);
        redBoots.setItemMeta(redBootsMeta);

        suitsInv.setItem(0, redHelmet);
        suitsInv.setItem(9, redChestplate);
        suitsInv.setItem(18, redLeggings);
        suitsInv.setItem(27, redBoots);
        player.openInventory(suitsInv);
    }
}
