package io.github.arraylists.cosmetics.menus;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.utils.HatGenerator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

public class CosmeticMenu {

    private CosmeticMenu() { }

    private static CosmeticMenu instance = new CosmeticMenu();

    public static CosmeticMenu getInstance() {
        return instance;
    }

    public Inventory cosmeticInv;

    public void openCosmetics(Player player) {
        cosmeticInv = Bukkit.getServer().createInventory(null, Main.getInstance().getConfig().getInt("cosmetics-inv-number-of-inventory-slots"), "Cosmetics");

        ItemStack gadgets = new ItemStack(Material.BLAZE_ROD);
        ItemMeta gadgetsMeta = gadgets.getItemMeta();

        ArrayList<String> gadgetsLore = new ArrayList<String>();
        gadgetsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-gadgets-inv-name")));
        gadgetsLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-gadgets-inv-lore")));
        gadgetsMeta.setLore(gadgetsLore);
        gadgets.setItemMeta(gadgetsMeta);

        ItemStack suits = new ItemStack(Material.LEATHER_CHESTPLATE);
        ItemMeta suitsMeta = suits.getItemMeta();

        ArrayList<String> suitsLore = new ArrayList<String>();
        suitsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-suits-inv-name")));
        suitsLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-suits-inv-lore")));
        suitsMeta.setLore(suitsLore);
        suits.setItemMeta(suitsMeta);

        ItemStack hats = HatGenerator.getInstance().createHat("http://textures.minecraft.net/texture/b0bfc2577f6e26c6c6f7365c2c4076bccee653124989382ce93bca4fc9e39b");
        SkullMeta hatsMeta = (SkullMeta) hats.getItemMeta();

        ArrayList<String> hatsLore = new ArrayList<String>();
        hatsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-hats-inv-name")));
        hatsLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-hats-inv-lore")));
        hatsMeta.setLore(hatsLore);
        hats.setItemMeta(hatsMeta);

        ItemStack pets = new ItemStack(Material.LEASH);
        ItemMeta petsMeta = pets.getItemMeta();

        ArrayList<String> petsLore = new ArrayList<String>();
        petsMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-pets-inv-name")));
        petsLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-pets-inv-lore")));
        petsMeta.setLore(petsLore);
        pets.setItemMeta(petsMeta);

        ItemStack morphs = new ItemStack(Material.EGG);

        ItemStack banners = new ItemStack(Material.BANNER);
        BannerMeta bannersMeta = (BannerMeta) banners.getItemMeta();

        ArrayList<String> bannersLore = new ArrayList<String>();
        List<Pattern> patterns = new ArrayList<Pattern>();

        bannersMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-banners-inv-name")));
        bannersLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("cosmetics-banners-inv-lore")));
        bannersMeta.setBaseColor(DyeColor.BLUE);
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_DOWNRIGHT));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_DOWNLEFT));
        patterns.add(new Pattern(DyeColor.RED, PatternType.CROSS));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_CENTER));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_MIDDLE));
        patterns.add(new Pattern(DyeColor.RED, PatternType.STRAIGHT_CROSS));
        bannersMeta.setPatterns(patterns);
        bannersMeta.setLore(bannersLore);
        banners.setItemMeta(bannersMeta);

        cosmeticInv.setItem(10, gadgets);
        cosmeticInv.setItem(12, suits);
        cosmeticInv.setItem(14, banners);
        cosmeticInv.setItem(16, hats);
        cosmeticInv.setItem(30, morphs);
        cosmeticInv.setItem(32, pets);
        player.openInventory(cosmeticInv);
    }
}
