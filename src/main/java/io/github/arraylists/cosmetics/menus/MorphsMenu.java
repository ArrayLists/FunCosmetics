package io.github.arraylists.cosmetics.menus;

import io.github.arraylists.cosmetics.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class MorphsMenu {

    private MorphsMenu() { }

    private static MorphsMenu instance = new MorphsMenu();

    public static MorphsMenu getInstance() {
        return instance;
    }

    public Inventory morphsInv;

    public void openMorphs(Player player) {
        morphsInv = Bukkit.getServer().createInventory(null, Main.getInstance().getConfig().getInt("morphs-inv-number-of-inventory-slots"), "Morphs");

        ItemStack wolfMorph = new ItemStack(Material.BONE);
        ItemMeta wolfMorphMeta = wolfMorph.getItemMeta();

        ArrayList<String> launcherGadgetLore = new ArrayList<String>();
        wolfMorphMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("launcher-gadget-name-in-inv")));
        launcherGadgetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("launcher-gadget-lore-in-inv")));
        wolfMorphMeta.setLore(launcherGadgetLore);
        wolfMorph.setItemMeta(wolfMorphMeta);

        ItemStack reset = new ItemStack(Material.REDSTONE);
        ItemMeta resetMeta = reset.getItemMeta();

        ArrayList<String> resetLore = new ArrayList<String>();
        resetMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("reset-gadgets-name-in-inv")));
        resetLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("reset-gadgets-lore-in-inv")));
        resetMeta.setLore(resetLore);
        reset.setItemMeta(resetMeta);

        ItemStack goBack = new ItemStack(Material.ARROW);
        ItemMeta goBackMeta = goBack.getItemMeta();

        ArrayList<String> goBackLore = new ArrayList<String>();
        goBackMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("go-back-name-in-inv")));
        goBackLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("go-back-lore-in-inv")));
        goBackMeta.setLore(goBackLore);
        goBack.setItemMeta(goBackMeta);

        morphsInv.setItem(0, wolfMorph);
        morphsInv.setItem(Main.getInstance().getConfig().getInt("reset-gadgets-slot-in-inv"), reset);
        morphsInv.setItem(Main.getInstance().getConfig().getInt("gadgets-go-back-slot-in-inv"), goBack);
        player.openInventory(morphsInv);
    }
}
