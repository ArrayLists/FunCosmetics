package io.github.arraylists.cosmetics.menus;

import io.github.arraylists.cosmetics.Main;
import io.github.arraylists.cosmetics.utils.HatGenerator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

public class HatsMenu {

    private HatsMenu() {  }

    private static HatsMenu instance = new HatsMenu();

    public static HatsMenu getInstance() {
        return instance;
    }

    public Inventory hatsInv;

    public void openHats(Player player) {
        hatsInv = Bukkit.getServer().createInventory(null, Main.getInstance().getConfig().getInt("hats-inv-number-of-inventory-slots"), "Hats");

        // TODO: Complete.
        ItemStack turtleHat = HatGenerator.getInstance().createHat("http://textures.minecraft.net/texture/12e548408ab75d7df8e6d5d2446d90b6ec62aa4f7feb7930d1ee71eefddf6189");
        SkullMeta turtleHatMeta = (SkullMeta) turtleHat.getItemMeta();

        ArrayList<String> turtleHatLore = new ArrayList<String>();
        turtleHatMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("turtlehat-name-in-inv")));
        turtleHatLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("turtlehat-lore-in-inv")));
        turtleHatMeta.setLore(turtleHatLore);
        turtleHat.setItemMeta(turtleHatMeta);

        hatsInv.setItem(0, turtleHat);
        player.openInventory(hatsInv);
    }
}
