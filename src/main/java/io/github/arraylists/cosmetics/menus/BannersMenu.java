package io.github.arraylists.cosmetics.menus;

import io.github.arraylists.cosmetics.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import java.util.ArrayList;
import java.util.List;

public class BannersMenu {

    private BannersMenu() {  }

    private static BannersMenu instance = new BannersMenu();

    public static BannersMenu getInstance() {
        return instance;
    }

    public Inventory bannersInv;

    public void openBanners(Player player) {
        bannersInv = Bukkit.getServer().createInventory(null, Main.getInstance().getConfig().getInt("banners-inv-number-of-inventory-slots"), "Banners");

        ItemStack ukBanner = new ItemStack(Material.BANNER);
        BannerMeta ukBannerMeta = (BannerMeta) ukBanner.getItemMeta();

        ArrayList<String> ukBannerLore = new ArrayList<String>();
        List<Pattern> patterns = new ArrayList<Pattern>();

        ukBannerMeta.setDisplayName(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("uk-banner-name-in-inv")));
        ukBannerLore.add(ChatColor.translateAlternateColorCodes( '&', Main.getInstance().getConfig().getString("uk-banner-lore-in-inv")));
        ukBannerMeta.setBaseColor(DyeColor.BLUE);
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_DOWNRIGHT));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_DOWNLEFT));
        patterns.add(new Pattern(DyeColor.RED, PatternType.CROSS));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_CENTER));
        patterns.add(new Pattern(DyeColor.WHITE, PatternType.STRIPE_MIDDLE));
        patterns.add(new Pattern(DyeColor.RED, PatternType.STRAIGHT_CROSS));
        ukBannerMeta.setPatterns(patterns);
        ukBannerMeta.setLore(ukBannerLore);
        ukBanner.setItemMeta(ukBannerMeta);

        bannersInv.setItem(0, ukBanner);
        player.openInventory(bannersInv);
    }
}
