package io.github.arraylists.cosmetics;

import de.robingrether.idisguise.api.DisguiseAPI;
import io.github.arraylists.cosmetics.commands.FunCosmeticsCommand;
import io.github.arraylists.cosmetics.cosmetics.banners.UnitedKingdomBanner;
import io.github.arraylists.cosmetics.cosmetics.gadgets.Launcher;
import io.github.arraylists.cosmetics.cosmetics.gadgets.SnowballLauncher;
import io.github.arraylists.cosmetics.cosmetics.hats.Turtle;
import io.github.arraylists.cosmetics.cosmetics.morphs.WolfMorph;
import io.github.arraylists.cosmetics.cosmetics.suits.RedSuit;
import io.github.arraylists.cosmetics.listeners.onInteract;
import io.github.arraylists.cosmetics.listeners.onInventoryClick;
import io.github.arraylists.cosmetics.listeners.onJoin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;

public class Main extends JavaPlugin {

    private static Main plugin;
    public DisguiseAPI api;
    public ArrayList<String> gadgetsToggled = new ArrayList<String>();
    public HashMap<String, Integer> cooldownTime;
    public HashMap<String, BukkitRunnable> cooldownTask;

    @Override
    public void onEnable() {
        plugin = this;
        getLogger().info("Fun Cosmetics by ArrayLists has been enabled!");
        getLogger().info("Thanks for using Fun Cosmetics! If you have any suggestions, let me know!");
        getLogger().info("If you find any bugs, please let me know!");
        getLogger().info("Loading dependencies, commands, listeners and configuration file.");

        if (!Bukkit.getServer().getPluginManager().isPluginEnabled("iDisguise")) {
            getLogger().severe("Please install the iDisguise API!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
            return;
        } else {
            getLogger().info("Found iDisguise.");
        }
        api = getServer().getServicesManager().getRegistration(DisguiseAPI.class).getProvider();
        getConfig().options().copyDefaults(true);
        getConfig().options().copyHeader(true);
        cooldownTime = new HashMap<String, Integer>();
        cooldownTask = new HashMap<String, BukkitRunnable>();
        saveDefaultConfig();
        loadCommand();
        loadListeners();

        getLogger().info("Success.");
    }

    public static Main getInstance() {
        return plugin;
    }

    public void loadCommand() {
        getCommand("funcosmetics").setExecutor(new FunCosmeticsCommand(this));
    }

    public void loadListeners() {
        Bukkit.getServer().getPluginManager().registerEvents(new onInteract(this), this);
        Bukkit.getServer().getPluginManager().registerEvents(new onJoin(this), this);
        Bukkit.getServer().getPluginManager().registerEvents(new onInventoryClick(this), this);

        // Cosmetics
        Bukkit.getServer().getPluginManager().registerEvents(new Launcher(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new RedSuit(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new Turtle(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new UnitedKingdomBanner(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new WolfMorph(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new SnowballLauncher(), this);
    }
}
