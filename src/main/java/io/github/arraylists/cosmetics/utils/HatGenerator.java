package io.github.arraylists.cosmetics.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.UUID;

public class HatGenerator {

    private HatGenerator() {  }
    private static HatGenerator instance = new HatGenerator();

    public static HatGenerator getInstance() {
        return instance;
    }

    public ItemStack createHat(String url) {
        ItemStack hat = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta hatMeta = (SkullMeta) hat.getItemMeta();

        GameProfile localGameProfile = new GameProfile(UUID.randomUUID(), null);
        byte[] arrayOfByte = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
        localGameProfile.getProperties().put("textures", new Property("textures", new String(arrayOfByte)));
        Field field = null;
        try {
            field = hatMeta.getClass().getDeclaredField("profile");
            field.setAccessible(true);
            field.set(hatMeta, localGameProfile);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        hatMeta.setDisplayName("head");
        hat.setItemMeta(hatMeta);

        return hat;
    }
}
