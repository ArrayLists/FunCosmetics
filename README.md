# FunCosmetics

## About
FunCosmetics is a cosmetic plugin for Minecraft. It adds, gadgets, hats, morphs, mounts, pets and suits! It is currently under development. If you would like to help with please let me know and I will give you permissions to contribute!

## Downloads
Spigot: Coming soon
Direct URL: Coming soon

## Commands, Usage and Aliases

### Aliases:
- cosmetics
- funcosmetics

###Command Usage:
/funcosmetics open - Opens the Fun Cosmetics cosmetic selector!
/funcosmetics open <CosmeticType> - Opens a specified cosmetic type!
/funcosmetics list - Shows all the cosmetics!
/funcosmetics reload - Reloads the configuration file!
/funcosmetics admin - Shows the admin usage!

## Permissions
```
permissions:
  funcosmetics.admin:
    description: 'Allow the user to use all the cosmetic permissions'
    children:
      funcosmetics.use: true
      funcosmetics.gadgets.*: true
      funcosmetics.gadgets.launcher: true
      funcosmetics.reload: true
      funcosmetics.suits.redhelmet: true
      funcosmetics.suits.redchestplate: true
      funcosmetics.suits.redleggings: true
      funcosmetics.suits.redboots: true
      funcosmetics.suits.*: true
    default: op
  funcosmetics.gadgets.*:
    description: 'Allow the user to use all the gadgets'
    children:
      funcosmetics.gadgets.launcher: true
    default: op
  funcosmetics.suits.*:
    description: 'Allow the user to use all the suits'
    children:
      funcosmetics.suits.redhelmet: true
      funcosmetics.suits.redchestplate: true
      funcosmetics.suits.redleggings: true
      funcosmetics.suits.redboots: true
    default: op
  funcosmetics.use:
    description: 'Allow the user to use the cosmetic command and right click the cosmetic selector'
  funcosmetics.gadgets.launcher:
    description: 'Allow the user to use the launcher gadget'
  funcosmetics.reload:
    description: 'Allow the user to use the /cosmetics reload command'
  funcosmetics.suits.redhelmet:
    description: 'Allow the user to use the red helmet'
  funcosmetics.suits.redchestplate:
    description: 'Allow the user to use the red chestplate'
  funcosmetics.suits.redleggings:
    description: 'Allow the user to use the red leggings'
  funcosmetics.suits.redboots:
    description: 'Allow the user to use the red boots'
```

## Configuration File

```
# FunCosmetics configuration file. It contains, cosmetic select sounds, messages and more!
give-cosmetic-selector-on-join: true
cosmetic-selector-slot: 4
cosmetic-selector-lore: '&7Right click to open!'
cosmetic-selector-name: '&b&lFunCosmetics &8» &7Right Click'
gadget-select-sound: 'ORB_PICKUP'
gadget-slot: 5
# This has to be a multiple of 9!
cosmetics-inv-number-of-inventory-slots: 45
cosmetics-gadgets-inv-name: '&b&lGadgets &8» &7Left Click'
cosmetics-gadgets-inv-lore: '&7Left click to open!'
# This has to be a multiple of 9!
gadgets-inv-number-of-inventory-slots: 27
launcher-gadget-name-in-inv: '&5&lLauncher &8» &7Left Click'
launcher-gadget-lore-in-inv: '&7Left click to select!'
launcher-gadget-name: '&5&lLauncher &8» &7Right Click'
launcher-gadget-lore: '&7Right click to use!'
reset-gadgets-name-in-inv: '&bReset Gadgets &8» &7Left Click'
reset-gadgets-lore-in-inv: '&7Left click to select!'
go-back-name-in-inv: '&bGo Back &8» &7Left Click'
go-back-lore-in-inv: '&7Left click to select!'
gadgets-go-back-slot-in-inv: 23
reset-gadgets-slot-in-inv: 21
# This has to be a multiple of 9!
suits-inv-number-of-inventory-slots: 45
cosmetics-suits-inv-name: '&c&lSuits &8» &7Left Click'
cosmetics-suits-inv-lore: '&7Left click to open!'
redsuit-helmet-name-in-inv: '&c&lRed Helmet &8» &7Left Click'
redsuit-helmet-lore-in-inv: '&7Left click to select!'
redsuit-helmet-name: '&cRed Helmet'
redsuit-chestplate-name-in-inv: '&c&lRed Chestplate &8» &7Left Click'
redsuit-chestplate-lore-in-inv: '&7Left click to select!'
redsuit-chestplate-name: '&cRed Chestplate'
redsuit-leggings-name-in-inv: '&c&lRed Leggings &8» &7Left Click'
redsuit-leggings-lore-in-inv: '&7Left click to select!'
redsuit-leggings-name: '&cRed Leggings'
redsuit-boots-name-in-inv: '&c&lRed Boots &8» &7Left Click'
redsuit-boots-lore-in-inv: '&7Left click to select!'
redsuit-boots-name: '&cRed Boots'
# This has to be a multiple of 9!
hats-inv-number-of-inventory-slots: 27
cosmetics-hats-inv-name: '&6&lHats &8» &7Left Click'
cosmetics-hats-inv-lore: '&7Left click to open!'
turtlehat-name-in-inv: '&6&lTurtle Hat &8» &7Left Click'
turtlehat-lore-in-inv: '&7Left click to select!'
turtlehat-name: '&6Turtle'
# Make sure the sounds you input are actual in-game sound files! I suggest going online and finding them!
helmet-select-sound: 'ORB_PICKUP'
chestplate-select-sound: 'ORB_PICKUP'
leggings-select-sound: 'ORB_PICKUP'
boots-select-sound: 'ORB_PICKUP'
gadget-select-sound: 'ORB_PICKUP'
hat-select-sound: 'ORB_PICKUP'
not-a-player: '&b&lFunCosmetics &8» &cYou are not a player!'
no-perms: '&b&lFunCosmetics &8» &cNo permissions!'
usage-info-line-1: '&b&m----------&r &b&lFunCosmetics &8» &bUsage &b&m------'
usage-info-line-2: '&b/funcosmetics open &8- &bOpens the cosmetic menu.'
usage-info-line-3: '&b/funcosmetics open <Cosmetic Type> &8- &bOpens a cosmetic type.'
usage-info-line-4: '&b/funcosmetics toggle &8- &bToggles whether users can use gadgets on you or not.'
usage-info-line-5: '&b/funcosmetics list &8- &bShows a list of all the cosmetics.'
usage-info-line-6: '&b&m---------------------------------------------------------'
on-gadget-select: '&b&lFunCosmetics &8» &bYou successfully selected the &f%gadget% &bgadget!'
on-helmet-select: '&b&lFunCosmetics &8» &bYou successfully selected the &f%helmet%&b!'
on-chestplate-select: '&b&lFunCosmetics &8» &bYou successfully selected the &f%chestplate%&b!'
on-leggings-select: '&b&lFunCosmetics &8» &bYou successfully selected the &f%leggings%&b!'
on-boots-select: '&b&lFunCosmetics &8» &bYou successfully selected the &f%boots%&b!'
on-hat-select: '&b&lFunCosmetics &8» &bYou successfully selected the &f%hat% &bhat!'
target-gadgets-toggled: '&b&lFunCosmetics &8» &f%target% &bhas gadgets toggled!'
no-gadget-selected: '&b&lFunCosmetics &8» &cYou do not have a gadget selected!'
gadgets-toggled-off: '&b&lFunCosmetics &8» &bPlayers can now use gadgets on you!'
gadgets-toggled-on: '&b&lFunCosmetics &8» &bPlayers can no longer use gadgets on you!'
cool-down-time-left: '&b&lFunCosmetics &8» &bYou can use this gadget again in &f%time% &bsecond(s)!'
```

## Status
Under development